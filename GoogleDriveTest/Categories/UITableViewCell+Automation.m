#import "UITableViewCell+Automation.h"
#import <objc/runtime.h>

static void * kRegisteredCells;

@interface UITableViewCell (Private)
+(NSArray *)cellStylesStrings;
@end

@interface UITableView (Registration)
-(NSArray *)registeredCellNames;
-(BOOL)registerCell:(NSString *)nibName;
@end

@implementation UITableView (Registration)
-(NSArray *)registeredCellNames {
    return [NSArray arrayWithArray:objc_getAssociatedObject(self, kRegisteredCells)];
}
-(BOOL)registerCell:(NSString *)nibName {
    NSInteger styleIndex = [[UITableViewCell cellStylesStrings] indexOfObject:nibName];
    if (styleIndex != NSNotFound || [nibName isEqualToString:NSStringFromClass([UITableViewCell class])]) { return NO; }
    
    if (![[self registeredCellNames] containsObject:nibName]) {
        UINib *nib = [UINib nibWithNibName:nibName bundle:nil];
        if (nib) {
            [self registerNib:nib forCellReuseIdentifier:nibName];
            
        } else if (NSClassFromString(nibName)) {
            [self registerClass:NSClassFromString(nibName) forCellReuseIdentifier:nibName];
            
        } else {
            return NO;
        }
        
        NSArray *registered = [[self registeredCellNames] arrayByAddingObject:nibName];
        objc_setAssociatedObject(self, kRegisteredCells, registered, OBJC_ASSOCIATION_RETAIN);
    }
    
    return YES;
}
@end

@implementation UITableViewCell (Automation)
+(instancetype)instance:(UITableView *)tableView nibName:(NSString *)nibName indexPath:(NSIndexPath *)indexPath {
    if ([tableView registerCell:nibName]) {
        return [tableView dequeueReusableCellWithIdentifier:nibName forIndexPath:indexPath];
    }
    return [self fallbackInstance:tableView nibName:nibName indexPath:indexPath];
}
+(instancetype)sizingInstance:(UITableView *)tableView nibName:(NSString *)nibName {
    if ([tableView registerCell:nibName]) {
        return [tableView dequeueReusableCellWithIdentifier:nibName];
    }
    return [self fallbackInstance:tableView nibName:nibName indexPath:nil];
}

+(instancetype)fallbackInstance:(UITableView *)tableView nibName:(NSString *)nibName indexPath:(NSIndexPath *)indexPath {
    NSInteger styleIndex = [[self cellStylesStrings] indexOfObject:nibName];
    if (styleIndex != NSNotFound) {
        return [[UITableViewCell alloc] initWithStyle:[[self cellStyles][styleIndex] integerValue] reuseIdentifier:nibName];
    } else {
        return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nibName];
    }
}
+(NSArray *)cellStylesStrings {
    return @[@"UITableViewCellStyleDefault", @"UITableViewCellStyleValue1", @"UITableViewCellStyleValue2", @"UITableViewCellStyleSubtitle"];
}
+(NSArray *)cellStyles {
    return @[@(UITableViewCellStyleDefault), @(UITableViewCellStyleValue1), @(UITableViewCellStyleValue2), @(UITableViewCellStyleSubtitle)];
}
@end
