#import <UIKit/UIKit.h>

@interface UITableViewCell (Automation)
+(instancetype)instance:(UITableView *)tableView nibName:(NSString *)nibName indexPath:(NSIndexPath *)indexPath;
+(instancetype)sizingInstance:(UITableView *)tableView nibName:(NSString *)nibName;
@end
