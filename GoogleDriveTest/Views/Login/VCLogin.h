//
//  VCLogin.h
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GoogleAuthHandler;

typedef void(^loginDidSignIn)();

@interface VCLogin : UIViewController
+(instancetype)loginWithAuth:(GoogleAuthHandler *)auth;

@property (nonatomic, copy) loginDidSignIn loginBlock;
@end
