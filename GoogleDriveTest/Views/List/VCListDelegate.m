//
//  VCListDelegate.m
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import "VCListDelegate.h"

@implementation VCListDelegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *text = self.items[indexPath.row];
    cell.textLabel.text = text;
}
@end
