//
//  VCListViewModel.m
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import "VCListViewModel.h"
#import "GoogleAuthHandler.h"
#import "NSArray+Map.h"

@interface VCListViewModel ()
@property (nonatomic, strong) GoogleAuthHandler *auth;
@end

@implementation VCListViewModel
#pragma mark - Lifecycle 
-(instancetype)initWithAuth:(GoogleAuthHandler *)auth {
    if (!(self = [super init])) { return nil; }
    self.auth = auth;
    return self;
}

#pragma mark - Public
-(void)signOut {
    [self.auth signOut];
}
-(void)refreshData {
    [self queryForDriveFiles];
}

#pragma mark - Private - Events
-(void)didChangeWorkingState:(BOOL)working {
    if (self.workingBlock) { self.workingBlock(working); }
}
-(void)didError:(NSError *)error {
    if (self.errorBlock) { self.errorBlock(error); }
}
-(void)didUpdate:(NSArray *)items {
    if (self.updateBlock) { self.updateBlock(items); }
}

#pragma mark - Private
-(void)queryForDriveFiles {
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
    
    [self didChangeWorkingState:YES];
    [self.auth.googleDrive executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLDriveFileList *files, NSError *error) {
        [self didChangeWorkingState:NO];
        
        if (error) {
            [self didError:error];
            
        } else {
            NSArray *items = [files.items map:^id(GTLDriveFile *item) {
                return item.title;
            }];
            [self didUpdate:items];
        }
    }];
}
@end
