#import "VCList+Factory.h"
#import "AppDelegate.h"
#import "GoogleAuthHandler.h"
#import "VCListViewModel.h"

/*
 I have shown how to couple to AppDelegate here, but you could just as easily parse this dependency directly to the factory method
 */
@interface AppDelegate ()
@property (nonatomic, strong) GoogleAuthHandler *auth;
@end

@implementation VCList (Factory)
+(instancetype)factoryInstance {
    //get reference to dependency from AppDelegate
    GoogleAuthHandler *auth = ((AppDelegate *)[UIApplication sharedApplication].delegate).auth;
    
    VCListViewModel *viewModel = [[VCListViewModel alloc] initWithAuth:auth];
    return [VCList listWithViewModel:viewModel];
}
@end
