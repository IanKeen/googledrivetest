//
//  VCListDataSource.h
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCListDataSource : NSObject <UITableViewDataSource>
@property (nonatomic, strong) NSArray *items;
@end
